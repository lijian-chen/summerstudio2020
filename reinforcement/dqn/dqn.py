from collections import deque
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import backend as K
import tensorflow as tf
import numpy as np
import random

class DQN:
    def __init__(self, env):
        self.env = env
        self.memory = deque(maxlen=2000)
        self.gamma = 0.95
        self.epsilon = 0.9
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.995
        self.learning_rate = 0.01
        self.model = self.create_model()
        self.target_model = self.create_model()

    """Huber loss for Q Learning
        References: https://en.wikipedia.org/wiki/Huber_loss
                    https://www.tensorflow.org/api_docs/python/tf/losses/huber_loss
        """

    def huber_loss(self, pred, label, clip_delta=1.0):
        error = label - pred
        cond = K.abs(error) <= clip_delta

        squared_loss = 0.5 * K.square(error)
        quadratic_loss = 0.5 * K.square(clip_delta) + clip_delta * (K.abs(error) - clip_delta)

        return K.mean(tf.where(cond, squared_loss, quadratic_loss))

    def create_model(self):
        # We essentially take all the states and find out the ideal
        # score from the output to determine which action to take
        model = Sequential()
        state_shape = self.env.observation_space.shape
        model.add(Dense(40, input_dim=state_shape[0], activation='relu'))
        model.add(Dense(20, activation='relu'))
        model.add(Dense(self.env.action_space.n))
        model.compile(loss=self.huber_loss,
                      optimizer=Adam(lr=self.learning_rate))
        return model

    def act(self, state):
        self.epsilon *= self.epsilon_decay
        self.epsilon = max(self.epsilon_min, self.epsilon)
        if np.random.random() < self.epsilon:
            return self.env.action_space.sample()
        return np.argmax(self.model.predict(state)[0])

    def remember(self, state, action, reward, new_state, done):
        self.memory.append([state, action, reward, new_state, done])

    def replay(self):
        # take a sample of previous history and use it to train the DQN
        # from the sample, use it to train to the Q-learning function
        batch_size = 10
        if len(self.memory) // batch_size > 2:
            batch_size = 20
        if len(self.memory) // batch_size > 4:
            batch_size = 40
        if len(self.memory) < batch_size:
            return

        samples = random.sample(self.memory, batch_size)
        for sample in samples:
            state, action, reward, new_state, done = sample
            target = self.model.predict(state)
            if done:
                target[0][action] = reward
            else:
                q_future = np.amax(self.target_model.predict(new_state)[0])
                target[0][action] = reward + q_future * self.gamma
            self.model.fit(state, target, epochs=1, verbose=0)

    def update_target(self):
        # a strategy to assist in converging
        # we use two models
        # the normal model is used for predictions whilst the target model is our reference
        # we are essentially trying to get our normal model be like the target model
        # we update it when we have gotten success
        self.target_model.set_weights(self.model.get_weights())

    def save_model(self, fn):
        self.model.save(fn, overwrite=True)

    def load_model(self, fn):
        self.model.load_weights(fn)
