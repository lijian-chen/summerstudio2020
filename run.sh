#!/bin/bash
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
xhost + $ip

docker run --rm -it \
	-e DISPLAY=$ip:0 \
	--env="QT_X11_NO_MITSHM=1" \
	-p 8888:8888 \
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--volume="/home/isaac/Desktop/Uni/Studio/summerstudio2020:/root/studio/" \
	--volume="/home/isaac/Desktop/Uni/Studio/summerstudioprojectpog:/root/project/" \
	--privileged \
	benthepleb/summerstudio2020:latest \
	bash
