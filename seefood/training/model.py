from tensorflow.keras.layers import Dense, Flatten, Dropout
from tensorflow.keras.layers import GlobalAveragePooling2D
from tensorflow.keras.models import Model
from tensorflow.keras.applications import InceptionV3
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator

class CNN():
    def __init__(self, train_dir = 'data/train/', batch_size=32):
        # create some variables that are quick and dirty
        self.target_size = (299,299)
        self.batch_size = batch_size
        self.model = self.create_model()
        self.callbacks = self.create_callbacks()
        self.train_generator, self.val_generator = self.create_generators(train_dir)

    def create_model(self):
        pre_trained_model = InceptionV3(weights='imagenet', include_top=False)

        # Append the model with a pretrained network
        x = pre_trained_model.output
        x = GlobalAveragePooling2D()(x)
        x = Flatten()(x)
        x = Dense(1024, activation='relu')(x)
        x = Dense(1024, activation='relu')(x)
        x = Dropout(0.5)(x)
        x = Dense(1024, activation='relu')(x)
        x = Dense(1024, activation='relu')(x)
        output = Dense(1, activation='sigmoid')(x)
        model = Model(inputs=pre_trained_model.input, outputs=output)

        # Freeze the first 80% percent of the model
        layer_index = int(len(model.layers) * 0.7)

        for layer in model.layers[:layer_index]:
            layer.trainable = False
        for layer in model.layers[layer_index:]:
            layer.trainable = True

        # Compile the model
        model.compile(optimizer='sgd', loss='binary_crossentropy',
            metrics=['accuracy'])

        return model

    def create_callbacks(self):
        # creating callbacks to prevent overfitting and saving the best
        checkpoint = ModelCheckpoint('hotdog.h5', monitor='val_accuracy',
            verbose=1, save_best_only=True, mode='max')
        callback = EarlyStopping(monitor='val_loss', patience=15)
        return [checkpoint, callback]

    def create_generators(self, train_dir):
        # process the datagenerators in the same fashion
        datagen = ImageDataGenerator(preprocessing_function=preprocess_input,
            zoom_range = 0.2, validation_split=0.10, shear_range=0.2)
        train_generator = datagen.flow_from_directory(train_dir,
            target_size=self.target_size, class_mode='binary',
            batch_size=self.batch_size, shuffle=True, subset='training',
            classes=['not_hot_dog', 'hot_dog'])
        val_generator = datagen.flow_from_directory(train_dir,
            target_size=self.target_size, class_mode='binary',
            batch_size=self.batch_size, subset='validation',
            classes=['not_hot_dog', 'hot_dog'])

        return train_generator, val_generator

    def train(self, epochs=50):
        self.model.fit(self.train_generator, verbose=1,
            callbacks = self.callbacks, epochs=epochs,
            validation_data=self.val_generator)
